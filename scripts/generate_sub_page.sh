#!/usr/bin/env bash

if [ "x" == "x$1" ]; then
	echo "usage: $0 <filelist or wildcard>"
	exit 1
fi

COMMIT_DATE=${@:1:3}

echo Commit time is : $COMMIT_TIME > /dev/stderr
echo Generating html from file list: ${@:4} > /dev/stderr

cat << EndOfMessage
<!DOCTYPE html>
<html lang="en">
<head>
  	<meta charset="UTF-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<meta http-equiv="X-UA-Compatible" content="ie=edge">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<title>19A-ITT3-IoT subpage</title>
</head>
<body>
	<div class="container-fluid text-center">
    <div class="row">
        <h3 class="col-sm-12">Files available</h3>
    </div>
	<div class="btn-group-vertical text-center">
EndOfMessage

for F in ${@:4}; do
	BASE=`basename "$F"`
	echo "	<a class=\"btn btn-success btn-lg\" href=\"$BASE\">$F</a>"
done

cat << EndOfMessage
	</div>
	<div class="col-sm-12">generated @ $COMMIT_TIME</div>
</div>
</body>
</html>
EndOfMessage
