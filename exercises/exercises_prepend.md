---
title: '19A-ITT3-IoT'
subtitle: 'Exercises'
authors: ['Ruslan Trifonov \<rutr@ucl.dk\>']
main_author: 'Ruslan Trifonov '
date: \today
email: 'rutr@ucl.dk'
left-header: \today
right-header: '19A-ITT3-IoT, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans. 

References

* [Weekly plans](https://eal-itt.gitlab.io/19A-ITT3-IoT/19A-ITT3-IoT_weekly_plans.pdf)



