---
title: '19A-ITT3-IoT'
subtitle: 'Lecture plan'
authors: ['Ruslan Trifonov \<rutr@ucl.dk\>']
main_author: 'Ruslan Trifonov'
date: \today
email: 'rutr@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or IoT.

* Study program, class and semester: IT technology, oeait18, 19A
* Name of lecturer and date of filling in form: ILES, 2019-08-01
* Title of the course, module or IoT and ECTS: Embedded Systems, ? ECTS
* Required readings, literature or technical instructions and other background material: None

See weekly plan for details like detailed daily plan, links, references, exercises and so on.


-------- ------ ---------------------------------------------
 INIT     Week  Content
-------- ------ ---------------------------------------------
RUTR
---------------------------------------------------------


# General info about the course
The Course will be focusing on practical skill, examination of papers and project work on a IoT assignment with reference to GPS, localization and sensors. The project will be class assigment. Team of many experts model.
Where the students build an interface for several sensors used in drones.

## The student’s learning outcome

### Knowledge

Students will understand the planning of a IoT system. The challenges that it will represent to the set up one, as well as the fundamental theoretical knowledge.

### Skills

Students will develop skills in the IoT sector, as well as various techniques while looking into interfacing sensors with respect to localization.

### Competencies

Interfacing of motion sensors and GPS as applied in the robotic industry with focus on drones.

## Content

The course will be split into theoretical part that will give a base of knowledge and then focus on the practical part. Again focus of the practical work will be sensor technologies.

## Method

The course will have approximately 20 present of the time theory and the rest will be work on the assigned project.

## Equipment

Raspberry PI owned and provided by the students. Various sensors and GPS provided by UCL and the cooperation company.

## Course Project with external collaborators  

The course project will be in cooperation with Syd Dynamics.
More information will be provided when a final agreement is reached with Syd Dynamics.

## Test form/assessment
The assessment form is a presentation, with a pass/fall out come. No grade is given. The successful student can answer all the questions in the following topics.

### Interface
The student an defend his view and present his knowledge with respect to sensor interfacing. Focus is on sensors used in the project.

### IoT and security
The student can demonstrate knowledge around IoT and security. The consideration around security issues and possible solutions.

### Robotic technologies
The students has to demonstrate basic knowledge and understand of the robot used as a platform of demonstration and the carrier of the interfaced sensors.

## Other general information
This project is in cooperation with a company. The lecture expect to work together with the class every Friday.
