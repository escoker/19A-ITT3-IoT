---
title: '19A-ITT3-IOT Groups'
subtitle: 'Group list'
authors: ['Ruslan Trifonov \<rutr@ucl.dk\>']
main_author: 'Ruslan Trifonov'
date: \today
email: 'rutr@ucl.dk'
left-header: \today
right-header: 'ITT3 project, group list'
---

Introduction
====================

This document is compiled from the file `groups.txt` which includes the list of groups, and the file `group.md` from each repository.

The template for the file is

```
Group: <name>
-----------------

Members:

Pair one:
* <member A> <riot username>
* <member B> <riot username>
Pair two:
* <member C> <riot username>
* <member D> <riot username>

Juniper router: 

* Management SSH: <ip>:<port>
* External ip: <ip>

Raspberry 

* SSH access: <ip>:<port>
* REST API access: [http://<ip>:<port>](http://<ip>:<port>)

Group web server: 

* SSH access: <ip>:<port>
* REST API access: [http://<ip>:<port>](http://<ip>:<port>)



```

Newlines and specific formatting is important, and it is not allowed to add extra stuff.


Group list
================





